package com.xebia.monopoly.fitnesse.fixture.frontend;

import com.xebia.monopoly.pageobjects.AddUserPage;
import com.xebia.monopoly.pageobjects.PlayGamePage;

public class GoingToJailInFrontend {

    private PlayGamePage playGamePage;

    public void whenPlayerThrowDoublesThreeTimesInSuccession() {
        AddUserPage addUserPage = new AddUserPage();
        addUserPage.get();
        playGamePage = addUserPage.addUser("kishen").addUser("arjan").startGame();


        for (int i = 1; i <= 3; i++) {
            // implement rolling dice
        }
    }

    public boolean thenPlayerGoesToJail() {
        // implement checking going to jail
        return false;
    }
}
