package com.xebia.monopoly.fitnesse.fixture.frontend;

import com.xebia.monopoly.pageobjects.AddUserPage;
import com.xebia.monopoly.pageobjects.PlayGamePage;

import static org.junit.Assert.assertNotNull;

/**
 * FitNesse fixture.
 * @See http://fitnesse.org/FitNesse.UserGuide.WritingAcceptanceTests.SliM.DecisionTable
 */
public class MoveTokenInFrontend {

    private int die1, die2;
    private PlayGamePage playGamePage;

    /**
     * Called before a row is executed.
     */
    public void reset() {
        AddUserPage addUserPage = new AddUserPage();
        addUserPage.get();
        playGamePage = addUserPage.addUser("kishen").addUser("arjan").startGame();
    }

    public void setTurn(int turn) {
        for (int i = 1; i < turn; i++) {
            // implement rolling dice
        }
    }

    public void setDie1(int die1) {
        this.die1 = die1;
    }

    public void setDie2(int die2) {
        this.die2 = die2;
    }

    /**
     *  Called after all properties are set, before properties are read.
     */
    public void execute() {
        assertNotNull("playGamePage is not loaded", playGamePage);
        // implement rolling dice
    }

    public int newPosition() {
        assertNotNull("playGamePage is not loaded", playGamePage);
        // implement getting current position
        return -1;
    }

    public String anotherTurn() {
        assertNotNull("playGamePage is not loaded", playGamePage);
        // implement getting roll allowed
        return "ERROR";
    }

}
