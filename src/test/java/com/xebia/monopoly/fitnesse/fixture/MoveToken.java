package com.xebia.monopoly.fitnesse.fixture;

import com.xebia.monopoly.helpers.GameHelper;

import static com.xebia.monopoly.helpers.HelperFunctions.convertBooleanToYesOrNo;

/**
 * FitNesse fixture
 */
public class MoveToken {

    private GameHelper gameHelper = new GameHelper();
    private int die1, die2;

    /**
     * Clear data for each action.
     */
    public void reset() {
        gameHelper.setupGame();
    }

    public void setStartingPosition(int pos) {
        gameHelper.getPlayer().setCurrentPosition(pos);
    }

    public void setTurn(int turn) {
        gameHelper.jumpToTurn(turn);
    }

    public void setDie1(int die1) {
        this.die1 = die1;
    }

    public void setDie2(int die2) {
        this.die2 = die2;
    }

    /**
     *  Called after all properties are set, before properties are read.
     */
    public void execute() {
        gameHelper.setDice(die1, die2);
        gameHelper.doPlayAction();
    }

    public int newPosition() {
        return gameHelper.getNewPosition();
    }

    public String anotherTurn() {
        return convertBooleanToYesOrNo(gameHelper.getPlayer().isRollAllowed());
    }

}
