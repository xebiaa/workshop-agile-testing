@PassingGo
Feature: Passing Go
  As a player
  I want to receive salary when passing or landing on GO
  So that I can buy more properties.

  Scenario: Passing go
    When I pass Go
    Then I receive 200 dollars


  Scenario: Landing on go
    When I land on Go
    Then I receive 200 dollars
