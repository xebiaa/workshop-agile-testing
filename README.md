Workshop agile testing
======================

[![Build Status](https://travis-ci.org/xebia/workshop-agile-testing.svg?branch=master)](https://travis-ci.org/xebia/workshop-agile-testing)

This project contains Cucumber, FitNesse, Sikuli and Gatling tests, both for backend and frontend testing.

How to get SUT and test tooling up and running
------------------------------------------------

The workshop uses Gradle as build tool.

__Start SUT - Monopoly web application__

	$ ./gradlew appRun

__Start FitNesse__

	$ ./gradlew fitnesseRun

__Run Cucumber tests__

Testing the SUT through cucumber can be done in two ways. Through the commandline with the command below, or through the JUnit runner in your IDE. By using the commandline runner, the SUT gets started for the test (and shutdown right afterwards), so make sure it's not still running from earlier Fitnesse testing.

	$ ./gradlew cucumber



